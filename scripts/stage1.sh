#!/bin/sh
$CONTAINER_ROOT="$1"

echo "Creating pacman cache"
PACMAN_CACHE="$CONTAINER_ROOT/var/cache/pacman/pkg"
mkdir -p "$PACMAN_CACHE"

echo "Detecting architecture..."
case "$(uname -m)" in
    arm64|aarch64)
        REPO_ARCH="aarch64"
    arm*)
        REPO_ARCH="arm"
esac
echo "Detected architecture: $REPO_ARCH"
REPO="http://mirror.archlinuxarm.org/$REPO_ARCH/core"

echo "Installing dependencies for pacman..."
ARCH_PACKAGES=$(wget -q -O - "${ARCH_REPO}/" | sed -n '/<a / s/^.*<a [^>]*href="\([^\"]*\)".*$/\1/p' | awk -F'/' '{print $NF}' | sort -rn)

while read package_name; do
    echo "Installing $package_name"
done <../data/stage1.packages