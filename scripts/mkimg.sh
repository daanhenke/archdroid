#!/bin/sh
echo "Creating image: $1"
busybox dd if=/dev/zero of="$1" bs=1M seek=4095 count=0

echo "Formatting image: $1"
busybox mke2fs -F "$1"