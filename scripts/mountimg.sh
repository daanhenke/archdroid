#!/bin/sh
echo "Mounting image: $1"
mkdir -p "$2"
busybox mount "$1" "$2"